# How to build wrappers for new met4j-toolbox apps?

## Install planemo

```console
pip install virtualenv
python -m virtualenv planemo
. planemo/bin/activate
pip install --upgrade pip setuptools  
pip install planemo
```

## Install singularity

Doc [Here](https://github.com/sylabs/singularity/blob/main/INSTALL.md).

## Create new wrappers

- Create a branch (e.g. newFeature) (or a new release) in met4j and develop your apps
- Push the branch
- If this branch is not master or develop, launch manually the 
building of the singularity image in the met4j CI-CD (this singularity image will be downloaded by the galaxy instance)
- Create a branch with the same name in met4j-galaxy (e.g. newFeature)
- Use the GenerateGalaxyFiles of the met4j-toolbox (be careful, it must be the version of the new branch) to generate the wrappers for the new met4j apps. Or you can use the singularity image created in the third step.

```console
met4j-toolbox.sif GenerateGalaxyFiles \
 -o /path/to/met4j-galaxy/tools
```

## Write tests

- Edit manually the wrappers where the tests are lacking.

[Here](https://docs.galaxyproject.org/en/latest/dev/schema.html#tool-tests) is the documentation about tests in galaxy

Put the test files in a test-data directory in the tool directory.
If the file is used in several tests, put it in the data directory at the root of the project and create a symbolic link in the test-data directory.

## Check the syntax of the wrappers

```console
. planemo/bin/activate
planemo shed_lint --tools \
--ensure_metadata \
--report_level warn \
--fail_level error \
--recursive tools
```

## Find apps without tests

```console
python ./findToolsWithoutTests.py build/tools/
```

## Launch the tests

### Build with a version number

To test your apps, you need to fill the good version of met4j singularity in each xml wrapper. For this, use this command:

```bash
./build.sh $versionNumber
```

Where $versionNumber must correspond to the version of the met4j singularity instance created above.

### Launch the tests on the built instance

Launching all the tests can be very time consuming. 

You can launch the test for one tool (here convert.Sbml2Graph):

```console
. planemo/bin/activate
planemo test \
--job_config_file config/job_conf.xml \
build/tools/Sbml2Graph/
```

If you want to test all the tools:

```console
. planemo/bin/activate
planemo test \
--job_config_file config/job_conf.xml \
build/tools/*
```

The results of the tests can be read in tool_test_output.html.

## Create new release

When tests are ok, you can create a new met4j release. The CI will create a new Singularity image corresponding to this version.

Create a new met4j-galaxy release with the same version number as met4j. 

After testing this new version, you have to do: 

```bash
git push origin 1.3.0
```

Modify the 1.3.0 by the version number that you want to push.

After push, the CI will test again the apps. If it's ok, a new version of met4j-galaxy will be pushed on the galaxy toolshed. The instance in usegalaxy.fr will be updated the next monday.

## Update met4j-galaxy-runner

Docker image to run planemo on ci.

Creation du docker pour lancer galaxy et planemo :

```bash
sudo docker login
sudo docker build -t metexplore/met4j-galaxy-runner:latest .
sudo docker push metexplore/met4j-galaxy-runner:latest
```

<https://hub.docker.com/r/metexplore/met4j-galaxy-runner>