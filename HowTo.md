# How to build wrappers for new met4j-toolbox apps?

## Work environment

You need planemo to lint and test the wrappers on this projet.

### Use a devcontainer

Use the command `DevContainer : Rebuild an open in container` to develop with python, planemo, conda and git installed.

### Or : install planemo

```sh
pip install virtualenv
python -m virtualenv planemo
. planemo/bin/activate
pip install --upgrade pip setuptools  
pip install planemo
```

## Create new wrappers

- Create a branch (e.g. newFeature) (or a new release) in met4j and develop your apps
- Push the branch
- If this branch is not master or develop, launch manually the 
building of the singularity image in the met4j CI-CD (this singularity image will be downloaded by the galaxy instance)
- Create a branch with the same name in met4j-galaxy (e.g. newFeature)
- Use the GenerateGalaxyFiles of the met4j-toolbox (be careful, it must be the version of the new branch) to generate the wrappers for the new met4j apps. Or you can use the singularity image created in the third step.

```sh
met4j-toolbox.sif GenerateGalaxyFiles \
 -o /path/to/met4j-galaxy/tools
```

## Write tests

- Edit manually the wrappers where the tests are lacking.

[Here](https://docs.galaxyproject.org/en/latest/dev/schema.html#tool-tests) is the documentation about tests in galaxy

Put the test files in a test-data directory in the tool directory.
If the file is used in several tests, put it in the data directory at the root of the project and create a symbolic link in the test-data directory.

## Build the wrappers

```sh
./build.sh
```

This will build the final wrappers for all the tools defined in `/tools`. It will create a folder `/build` in which there will be a folder for every tool to push to the shed. Every folder will contain :
- a .xml file describing the tool itself
- a macros.xml containing the macros (variables similar to every tool like the requirement package or the tool version)
- a .shed.yml describing the name of the shed that will be pushed into the tool shed

## Check the syntax of the wrappers

```sh
. planemo/bin/activate
planemo shed_lint --tools \
--ensure_metadata \
--report_level warn \
--fail_level error \
--recursive build/
```

## Find apps without tests

```sh
python ./findToolsWithoutTests.py build/
```

## Launch the tests

Launching all the tests can be very time consuming. 

You can launch the test for one tool (here convert.Sbml2Graph):

```sh
. planemo/bin/activate
planemo test build/Sbml2Graph/
```

If you want to test all the tools:

```sh
. planemo/bin/activate
planemo test build/*
```

The results of the tests can be read in tool_test_output.html.

## Open a Galaxy interface

If you want to test the tool manually, you can run a fake Galaxy instance for a tool :

```sh
planemo serve build/Sbml2Graph
```

This will run Galaxy on port 9090.

## Create new release

When tests are ok, you can create a new met4j release. The CI will create a new Singularity image corresponding to this version.

Create a new met4j-galaxy release with the same version number as met4j. 

After testing this new version, you have to do: 

```bash
git push origin <new_version>
```

After push, the CI will test again the apps. If it's ok, a new version of met4j-galaxy will be pushed on the galaxy toolshed. The instance in usegalaxy.fr will be updated the next monday.

## Update met4j-galaxy-runner

Docker image to run planemo on ci.

Creation du docker pour lancer galaxy et planemo :

```bash
sudo docker login
sudo docker build -t metexplore/met4j-galaxy-runner:latest .
sudo docker push metexplore/met4j-galaxy-runner:latest
```

<https://hub.docker.com/r/metexplore/met4j-galaxy-runner>