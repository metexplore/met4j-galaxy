import os
import xml.etree.ElementTree as ET

def find_xml_files_without_tests(directory):
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)
        if os.path.isdir(filepath):
            # Récursivement, recherche dans les sous-répertoires
            yield from find_xml_files_without_tests(filepath)
        elif filename.endswith(".xml"):
            # Traitement du fichier XML
            tree = ET.parse(filepath)
            root = tree.getroot()
            tool_elements = root.findall(".//tool")
            tests_elements = root.findall(".//tests")
            if tool_elements and not tests_elements:
                yield filepath
            elif tests_elements:
                # On vérifie qu'il n'y a rien à l'intérieur de <tests>
                tests_children = list(tests_elements[0])
                if len(tests_children) == 0:
                    yield filepath

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Trouve les fichiers XML sans élément <tests>")
    parser.add_argument("directory", metavar="DIRECTORY", type=str, help="Le répertoire à explorer")
    args = parser.parse_args()

    for filepath in find_xml_files_without_tests(args.directory):
        print(filepath)
