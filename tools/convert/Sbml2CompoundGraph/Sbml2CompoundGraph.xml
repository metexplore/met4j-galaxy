<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<tool id="met4j_Sbml2CompoundGraph" name="Sbml2CompoundGraph" version="2.0.1">
  <description>Advanced creation of a compound graph representation of a SBML file content</description>
  <xrefs>
    <xref type="bio.tools">met4j</xref>
  </xrefs>
  <requirements>
    <container type="singularity">oras://registry.forgemia.inra.fr/metexplore/met4j/met4j-singularity:2.0.1</container>
  </requirements>
  <command detect_errors="exit_code"><![CDATA[sh /usr/bin/met4j.sh convert.Sbml2CompoundGraph -i "$inputPath"
#if str($inputSide) != 'None':
 -sc "$inputSide"
#end if
#if str($mergingStrat):
 -mc "$mergingStrat"
#end if
 $mergeEdges
 $removeIsolated
 $degree
#if str($weightFile) != 'None':
 -cw "$weightFile"
#end if
 $undirected
 $computeWeight
#if str($format):
 -f "$format"
#end if
 -o "$output"
]]></command>
  <inputs>
    <param argument="-i" format="sbml" label="input SBML file" name="inputPath" optional="false" type="data" value=""/>
    <param argument="-sc" format="txt" label="input Side compound file" name="inputSide" optional="true" type="data" value=""/>
    <param argument="-mc" label="merge compartments. Use names if consistent and unambiguous across compartments, or identifiers if compartment suffix is present (id in form &quot;xxx_y&quot; with xxx as base identifier and y as compartment label)." name="mergingStrat" optional="true" type="select" value="no">
      <option selected="true" value="no">no</option>
      <option value="by_name">by_name</option>
      <option value="by_id">by_id</option>
    </param>
    <param argument="-me" checked="false" falsevalue="" label="merge parallel edges to produce a simple graph" name="mergeEdges" truevalue="-me" type="boolean" value="false"/>
    <param argument="-ri" checked="false" falsevalue="" label="remove isolated nodes" name="removeIsolated" truevalue="-ri" type="boolean" value="false"/>
    <param argument="-dw" checked="false" falsevalue="" label="penalize traversal of hubs by using degree square weighting" name="degree" truevalue="-dw" type="boolean" value="false"/>
    <param argument="-cw" format="tsv" label="an optional file containing weights for compound pairs" name="weightFile" optional="true" type="data" value=""/>
    <param argument="-un" checked="false" falsevalue="" label="create as undirected" name="undirected" truevalue="-un" type="boolean" value="false"/>
    <param argument="-tp" checked="false" falsevalue="" label="set weight as random walk transition probability, normalized by reaction" name="computeWeight" truevalue="-tp" type="boolean" value="false"/>
    <param argument="-f" label="Format of the exported graphTabulated edge list by default (source id &#9; edge type &#9; target id). Other options include GML, JsonGraph, and tabulated node list (label &#9; node id &#9; node type)." name="format" optional="true" type="select" value="tab">
      <option value="gml">gml</option>
      <option selected="true" value="tab">tab</option>
      <option value="nodeList">nodeList</option>
      <option value="json">json</option>
      <option value="matrix">matrix</option>
    </param>
  </inputs>
  <outputs>
    <data format="txt" name="output"/>
  </outputs>
  <tests>
    <test>
            
      <param name="inputPath" value="XF_network.sbml"/>
            
      <param name="inputSide" value="XF_network_C_Side.tab"/>
            
      <param name="format" value="gml"/>
            
      <output name="output">
                
        <assert_contents>
                    
          <has_line_matching expression=".*node.*" n="1101"/>
                    
          <has_line_matching expression=".*edge.*" n="5581"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
    <test>
            
      <param name="inputPath" value="XF_network.sbml"/>
            
      <param name="inputSide" value="XF_network_C_Side.tab"/>
            
      <param name="undirected" value="true"/>
            
      <param name="computeWeight" value="true"/>
            
      <param name="removeIsolated" value="true"/>
            
      <param name="degree" value="true"/>
            
      <param name="mergingStrat" value="by_id"/>
            
      <param name="format" value="gml"/>
            
      <output name="output">
                
        <assert_contents>
                    
          <has_line_matching expression=".*node.*" n="793"/>
                    
          <has_line_matching expression=".*edge.*" n="9372"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
  </tests>
  <help><![CDATA[Metabolic networks used for quantitative analysis often contain links that are irrelevant for graph-based structural analysis. For example, inclusion of side compounds or modelling artifacts such as 'biomass' nodes.
While Carbon Skeleton Graph offer a relevant alternative topology for graph-based analysis, it requires compounds' structure information, usually not provided in model, and difficult to retrieve for model with sparse cross-reference annotations.
In contrary to the Sbml2Graph app that performs a raw conversion of the SBML content, the present app propose a fine-tuned creation of compound graph from predefined list of side compounds and degree weighting to get relevant structure without structural data.This app also enables Markov-chain based analysis of metabolic networks by computing reaction-normalized transition probabilities on the network.]]></help>
  <citations/>
</tool>
