<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<tool id="met4j_SetCharges" name="SetCharges" version="2.0.1">
  <description>Set charge to metabolites in a SBML file from a tabulated file containing the metabolite ids and the charges</description>
  <xrefs>
    <xref type="bio.tools">met4j</xref>
  </xrefs>
  <requirements>
    <container type="singularity">oras://registry.forgemia.inra.fr/metexplore/met4j/met4j-singularity:2.0.1</container>
  </requirements>
  <command detect_errors="exit_code"><![CDATA[sh /usr/bin/met4j.sh attributes.SetCharges#if str($colcharge) != 'nan':
 -cc "$colcharge"
#end if
#if str($colid) != 'nan':
 -ci "$colid"
#end if
 $p
 $s
#if str($nSkip):
 -n "$nSkip"
#end if
 -i "$sbml"
#if str($tab) != 'None':
 -tab "$tab"
#end if
#if str($c):
 -c "$c"
#end if
 -o "$out"
]]></command>
  <inputs>
    <param argument="-cc" label="[2] number of the column where are the charges" name="colcharge" optional="true" type="integer" value="2"/>
    <param argument="-ci" label="[1] number of the column where are the metabolite ids" name="colid" optional="true" type="integer" value="1"/>
    <param argument="-p" checked="false" falsevalue="" label="[deactivated] To match the objects in the sbml file, adds the prefix M_ to metabolite ids" name="p" truevalue="-p" type="boolean" value="false"/>
    <param argument="-s" checked="false" falsevalue="" label="[deactivated] To match the objects in the sbml file, adds the suffix _comparmentID to metabolites" name="s" truevalue="-s" type="boolean" value="false"/>
    <param argument="-n" label="[0] Number of lines to skip at the beginning of the tabulated file" name="nSkip" optional="true" type="text" value="0">
      <sanitizer invalid_char="_">
        <valid initial="string.printable"/>
      </sanitizer>
    </param>
    <param argument="-i" format="sbml" label="Original SBML file" name="sbml" optional="false" type="data" value=""/>
    <param argument="-tab" format="tsv" label="Input Tabulated file" name="tab" optional="true" type="data" value=""/>
    <param argument="-c" label="[#] Comment String in the tabulated file. The lines beginning by this string won't be read" name="c" optional="true" type="text" value="#">
      <sanitizer invalid_char="_">
        <valid initial="string.printable"/>
      </sanitizer>
    </param>
  </inputs>
  <outputs>
    <data format="sbml" name="out"/>
  </outputs>
  <tests>
    <test>
            
      <param name="sbml" value="toy_model.xml"/>
            
      <param name="tab" value="charges.tsv"/>
            
      <output ftype="sbml" name="out">
                
        <assert_contents>
                    
          <is_valid_xml/>
                    
          <has_line_matching expression=".*fbc:charge=.2.*" n="1"/>
                    
          <has_line_matching expression=".*fbc:charge=.-3.*" n="1"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
    <test>
            
      <param name="sbml" value="toy_model.xml"/>
            
      <param name="tab" value="chargesWithComment.tsv"/>
            
      <output ftype="sbml" name="out">
                
        <assert_contents>
                    
          <is_valid_xml/>
                    
          <has_line_matching expression=".*fbc:charge=.2.*" n="1" negate="true"/>
                    
          <has_line_matching expression=".*fbc:charge=.-3.*" n="1"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
    <test>
            
      <param name="sbml" value="toy_model.xml"/>
            
      <param name="tab" value="charges.tsv"/>
            
      <param name="nSkip" value="1"/>
            
      <output ftype="sbml" name="out">
                
        <assert_contents>
                    
          <is_valid_xml/>
                    
          <has_line_matching expression=".*fbc:charge=.2.*" n="1" negate="true"/>
                    
          <has_line_matching expression=".*fbc:charge=.-3.*" n="1"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
    <test>
            
      <param name="sbml" value="toy_model.xml"/>
            
      <param name="tab" value="charges.tsv"/>
            
      <param name="ci" value="2"/>
            
      <param name="cc" value="3"/>
            
      <output ftype="sbml" name="out">
                
        <assert_contents>
                    
          <is_valid_xml/>
                    
          <has_line_matching expression=".*fbc:charge=.2.*" n="1"/>
                    
          <has_line_matching expression=".*fbc:charge=.-3.*" n="1"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
    <test>
            
      <param name="sbml" value="XF_network.sbml"/>
            
      <param name="tab" value="chargesXF.tsv"/>
            
      <param name="p" value="true"/>
            
      <param name="s" value="true"/>
            
      <output ftype="sbml" name="out">
                
        <assert_contents>
                    
          <is_valid_xml/>
                    
          <has_line_matching expression=".*fbc:charge=.-1000.*" n="3"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
  </tests>
  <help><![CDATA[Set charge to metabolites in a SBML file from a tabulated file containing the metabolite ids and the charges
The charge must be a number. The ids must correspond between the tabulated file and the SBML file.
If prefix or suffix is different in the SBML file, use the -p or the -s options.
The charge will be written in the SBML file in two locations:+
- in the reaction notes (e.g. charge: -1)
- as fbc attribute (e.g. fbc:charge="1")]]></help>
  <citations>
    <citation type="doi">10.1515/jib-2017-0082</citation>
  </citations>
</tool>
