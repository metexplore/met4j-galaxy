Creator "JGraphT GML Exporter"
Version 1
graph
[
	label ""
	directed 1
	node
	[
		id 1
		label "A"
	]
	node
	[
		id 2
		label "reac2"
	]
	node
	[
		id 3
		label "B"
	]
	node
	[
		id 4
		label "reac4"
	]
	node
	[
		id 5
		label "D"
	]
	edge
	[
		source 1
		target 2
		label "(A : reac2)"
	]
	edge
	[
		source 2
		target 3
		label "(reac2 : B)"
	]
	edge
	[
		source 3
		target 4
		label "(B : reac4)"
	]
	edge
	[
		source 4
		target 5
		label "(reac4 : D)"
	]
]
