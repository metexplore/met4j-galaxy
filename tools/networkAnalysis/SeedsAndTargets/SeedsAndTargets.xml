<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<tool id="met4j_SeedsAndTargets" name="SeedsAndTargets" version="2.0.1">
  <description>Identify exogenously acquired compounds, exogenously available producible compounds and/or dead ends metabolites from metabolic network topology</description>
  <xrefs>
    <xref type="bio.tools">met4j</xref>
  </xrefs>
  <requirements>
    <container type="singularity">oras://registry.forgemia.inra.fr/metexplore/met4j/met4j-singularity:2.0.1</container>
  </requirements>
  <command detect_errors="exit_code"><![CDATA[sh /usr/bin/met4j.sh networkAnalysis.SeedsAndTargets -i "$inputPath"
#if str($inputSide) != 'None':
 -sc "$inputSide"
#end if
#if str($comp):
 -c "$comp"
#end if
 $source
 $sink
 $notsource
 $notsink
 $keepIsolated
 $useBorensteinAlg
 $useInternal
 -o "$outputPath"
]]></command>
  <inputs>
    <param argument="-i" format="sbml" label="input SBML file" name="inputPath" optional="false" type="data" value=""/>
    <param argument="-sc" format="txt" label="input side compound file" name="inputSide" optional="true" type="data" value=""/>
    <param argument="-c" label="selected compartment(s), as model identifiers, separated by &quot;+&quot; sign if more than one" name="comp" optional="true" type="text" value="">
      <sanitizer invalid_char="_">
        <valid initial="string.printable"/>
      </sanitizer>
    </param>
    <param argument="-s" checked="false" falsevalue="" label="export seeds" name="source" truevalue="-s" type="boolean" value="false"/>
    <param argument="-t" checked="false" falsevalue="" label="export targets" name="sink" truevalue="-t" type="boolean" value="false"/>
    <param argument="-!s" checked="false" falsevalue="" label="export nodes that are not seeds" name="notsource" truevalue="-!s" type="boolean" value="false"/>
    <param argument="-!t" checked="false" falsevalue="" label="export nodes that are not targets" name="notsink" truevalue="-!t" type="boolean" value="false"/>
    <param argument="-is" checked="false" falsevalue="" label="do not ignore isolated nodes, consider isolated both seeds and targets" name="keepIsolated" truevalue="-is" type="boolean" value="false"/>
    <param argument="-B" checked="false" falsevalue="" label="use Borenstein Algorithm. Please cite Borenstein et al. 2008 Large-scale reconstruction and phylogenetic analysis of metabolic environments https://doi.org/10.1073/pnas.0806162105), ignore internal option" name="useBorensteinAlg" truevalue="-B" type="boolean" value="false"/>
    <param argument="-in" checked="false" falsevalue="" label="if an external compartment is defined, adjust degree by considering internal counterpart" name="useInternal" truevalue="-in" type="boolean" value="false"/>
  </inputs>
  <outputs>
    <data format="tsv" name="outputPath"/>
  </outputs>
  <tests>
    <test>
            
      <param name="inputPath" value="XF_network.sbml"/>
            
      <param name="source" value="true"/>
            
      <output ftype="tsv" name="outputPath">
                
        <assert_contents>
                    
          <has_n_columns n="1"/>
                    
          <has_n_lines n="109"/>
                    
          <has_line_matching expression="M_\S+_\w" n="109"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
    <test>
            
      <param name="inputPath" value="XF_network.sbml"/>
            
      <param name="sink" value="true"/>
            
      <param name="useInternal" value="true"/>
            
      <param name="comp" value="e"/>
            
      <output ftype="tsv" name="outputPath">
                
        <assert_contents>
                    
          <has_n_columns n="1"/>
                    
          <has_n_lines n="5"/>
                    
          <has_line_matching expression="^M_\S+_\w" n="5"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
    <test>
            
      <param name="inputPath" value="XF_network.sbml"/>
            
      <param name="sink" value="true"/>
            
      <param name="useBorensteinAlg" value="true"/>
            
      <param name="comp" value="e"/>
            
      <output ftype="tsv" name="outputPath">
                
        <assert_contents>
                    
          <has_n_columns n="1"/>
                    
          <has_n_lines n="26"/>
                    
          <has_line_matching expression="^M_\S+_\w" n="26"/>
                  
        </assert_contents>
              
      </output>
          
    </test>
  </tests>
  <help><![CDATA[Identify exogenously acquired compounds, exogenously available producible compounds and/or dead ends metabolites from metabolic network topology
Metabolic seeds and targets are useful for identifying medium requirements and metabolic capability, and thus enable analysis of metabolic ties within communities of organisms.
This application can use seed definition and SCC-based detection algorithm by Borenstein et al. or, alternatively, degree-based sink and source detection with compartment adjustment.
The first method (see Borenstein et al. 2008 Large-scale reconstruction and phylogenetic analysis of metabolic environments https://doi.org/10.1073/pnas.0806162105) consider strongly connected components rather than individual nodes, thus, members of cycles can be considered as seeds. A sink from an external compartment can however be connected to a non sink internal counterpart, thus highlighting what could end up in the external compartment rather than what must be exported.
The second approach is neighborhood based and identify sources and sinks. Since "real" sinks and sources in intracellular compartment(s) may be involved in transport/exchange reactions reversible by default, thus not allowing extracellular source or sink, an option allows to take the degree (minus extracellular neighbors) of intracellular counterparts.]]></help>
  <citations>
    <citation type="doi">10.1073/pnas.0806162105</citation>
  </citations>
</tool>
