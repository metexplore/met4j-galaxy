<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<tool id="met4j_TopologicalPathwayAnalysis" name="TopologicalPathwayAnalysis" version="@TOOL_VERSION@">
  <description>Run a Topological Pathway Analysis (TPA) to identify key pathways based on topological properties of its constituting compounds.</description>
  <macros>
    <import>macros.xml</import>
  </macros>
  <expand macro="bio_tools"/>
  <expand macro="requirements"/>
  <command detect_errors="exit_code"><![CDATA[met4j fr.inrae.toulouse.metexplore.met4j_toolbox.networkAnalysis.TopologicalPathwayAnalysis -i "$inputPath"
#if str($inputSide) != 'None':
 -sc "$inputSide"
#end if
#if str($mergingStrat):
 -mc "$mergingStrat"
#end if
 $removeIsolated
#if str($weightFile) != 'None':
 -cw "$weightFile"
#end if
 $undirected
 -noi "$dataPath"
 $out
 -o "$outputPath"
]]></command>
  <inputs>
    <param argument="-i" format="sbml" label="input SBML file" name="inputPath" optional="false" type="data" value=""/>
    <param argument="-sc" format="txt" label="input Side compound file (recommended)" name="inputSide" optional="true" type="data" value=""/>
    <param argument="-mc" label="merge compartments. Use names if consistent and unambiguous across compartments, or identifiers if compartment suffix is present (id in form &quot;xxx_y&quot; with xxx as base identifier and y as compartment label)." name="mergingStrat" optional="true" type="select" value="no">
      <option selected="true" value="no">no</option>
      <option value="by_name">by_name</option>
      <option value="by_id">by_id</option>
    </param>
    <param argument="-ri" checked="false" falsevalue="" label="remove isolated nodes" name="removeIsolated" truevalue="-ri" type="boolean" value="false"/>
    <param argument="-cw" format="tsv" label="an optional file containing weights for compound pairs, taken into account for betweenness computation. Edges not found in file will be removed" name="weightFile" optional="true" type="data" value=""/>
    <param argument="-un" checked="false" falsevalue="" label="the compound graph built from the metabolic network and used for computations will undirected, i.e. the reaction directions won't be taken into account" name="undirected" truevalue="-un" type="boolean" value="false"/>
    <param argument="-noi" format="txt" label="file containing the list of metabolites of interests (one per line)" name="dataPath" optional="false" type="data" value=""/>
    <param argument="-out" checked="false" falsevalue="" label="use out-degree as scoring function instead of betweenness (faster computation)" name="out" truevalue="-out" type="boolean" value="false"/>
  </inputs>
  <outputs>
    <data format="tsv" name="outputPath"/>
  </outputs>
  <tests>
    <test>
      <param name="inputPath" value="XF_network.sbml"/>
      <param name="dataPath" value="XF_network_C_NOI.txt"/>
      <param name="inputSide" value="XF_network_C_Side.tab"/>
      <output name="outputPath">
        <assert_contents>
          <has_n_columns n="3"/>
          <has_n_lines n="3"/>
        </assert_contents>
      </output>
    </test>
    <test>
      <param name="inputPath" value="XF_network.sbml"/>
      <param name="dataPath" value="XF_network_C_NOI.txt"/>
      <param name="inputSide" value="XF_network_C_Side.tab"/>
      <param name="undirected" value="true"/>
      <param name="removeIsolated" value="true"/>
      <param name="out" value="true"/>
      <param name="mergingStrat" value="by_id"/>
      <output name="outputPath">
        <assert_contents>
          <has_n_columns n="3"/>
          <has_n_lines n="3"/>
        </assert_contents>
      </output>
    </test>
  </tests>
  <help><![CDATA[Run a Topological Pathway Analysis (TPA) to identify key pathways based on topological properties of its constituting compounds.
From a list of compounds of interest, the app compute their betweenness centrality (which quantifies how often a compound acts as a intermediary along the shortest paths between pairs of other compounds in the network, which, if high, suggest a critical role in the overall flow within the network). Each pathway is scored according to the summed centrality of its metabolites found in the dataset. Alternatively to the betweenness, one can make use of the out-degree (the number of outgoing link, i.e. number of direct metabolic product) as a criterion of importance. TPA is complementary to statistical enrichment analysis to ensures a more meaningful interpretation of the data, by taking into account the influence of identified compounds on the structure of the pathways.
@ATTRIBUTION@]]></help>
  <citations/>
</tool>
