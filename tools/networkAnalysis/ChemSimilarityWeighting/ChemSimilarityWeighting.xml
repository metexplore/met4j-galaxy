<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<tool id="met4j_ChemSimilarityWeighting" name="ChemSimilarityWeighting" version="2.0.1">
  <description>Provides tabulated compound graph edge list, with one column with reactant pair's chemical similarity.</description>
  <xrefs>
    <xref type="bio.tools">met4j</xref>
  </xrefs>
  <requirements>
    <container type="singularity">oras://registry.forgemia.inra.fr/metexplore/met4j/met4j-singularity:2.0.1</container>
  </requirements>
  <command detect_errors="exit_code"><![CDATA[sh /usr/bin/met4j.sh networkAnalysis.ChemSimilarityWeighting#if str($type):
 -f "$type"
#end if
#if str($smileFile):
 -sm "$smileFile"
#end if
#if str($inchiFile):
 -in "$inchiFile"
#end if
 $dist
 -s "$inputPath"
#if str($inputSide) != 'None':
 -sc "$inputSide"
#end if
#if str($mergingStrat):
 -mc "$mergingStrat"
#end if
 $mergeEdges
 $undirected
 $computeWeight
 $removeNaN
 -o "$outputPath"
]]></command>
  <inputs>
    <param argument="-f" label="The chemical fingerprint to use" name="type" optional="true" type="select" value="Extended">
      <option value="EState">EState</option>
      <option selected="true" value="Extended">Extended</option>
      <option value="KlekotaRoth">KlekotaRoth</option>
      <option value="MACCS">MACCS</option>
      <option value="PubChem">PubChem</option>
    </param>
    <param argument="-sm" label="If not present in SBML's annotations, get structure from a tabulated file with first column as compound id and second column as SMILE string, no header. Ignored if inchi file is provided" name="smileFile" optional="true" type="text" value="">
      <sanitizer invalid_char="_">
        <valid initial="string.printable"/>
      </sanitizer>
    </param>
    <param argument="-in" label="If not present in SBML's annotations, get structure from a tabulated file with first column as compound id and second column as InChI string, no header." name="inchiFile" optional="true" type="text" value="">
      <sanitizer invalid_char="_">
        <valid initial="string.printable"/>
      </sanitizer>
    </param>
    <param argument="-d" checked="false" falsevalue="" label="Use distance rather than similarity" name="dist" truevalue="-d" type="boolean" value="false"/>
    <param argument="-s" format="sbml" label="input SBML file" name="inputPath" optional="false" type="data" value=""/>
    <param argument="-sc" format="txt" label="input Side compound file" name="inputSide" optional="true" type="data" value=""/>
    <param argument="-mc" label="merge compartments. Use names if consistent and unambiguous across compartments, or identifiers if compartment suffix is present (id in form &quot;xxx_y&quot; with xxx as base identifier and y as compartment label)." name="mergingStrat" optional="true" type="select" value="no">
      <option selected="true" value="no">no</option>
      <option value="by_name">by_name</option>
      <option value="by_id">by_id</option>
    </param>
    <param argument="-me" checked="false" falsevalue="" label="merge parallel edges to produce a simple graph" name="mergeEdges" truevalue="-me" type="boolean" value="false"/>
    <param argument="-un" checked="false" falsevalue="" label="create as undirected" name="undirected" truevalue="-un" type="boolean" value="false"/>
    <param argument="-tp" checked="false" falsevalue="" label="set weight as random walk transition probability, normalized by reaction" name="computeWeight" truevalue="-tp" type="boolean" value="false"/>
    <param argument="-nan" checked="false" falsevalue="" label="do not output edges with undefined weight" name="removeNaN" truevalue="-nan" type="boolean" value="false"/>
  </inputs>
  <outputs>
    <data format="tsv" name="outputPath"/>
  </outputs>
  <tests/>
  <help><![CDATA[Provides tabulated compound graph edge list, with one column with reactant pair's chemical similarity.Chemical similarity has been proposed as edge weight for finding meaningful paths in metabolic networks, using shortest (lightest) path search.]]></help>
  <citations>
    <citation type="doi">10.1093/bioinformatics/btg217</citation>
    <citation type="doi">10.1093/bioinformatics/btu760</citation>
    <citation type="doi">10.1093/bioinformatics/bti116</citation>
  </citations>
</tool>
