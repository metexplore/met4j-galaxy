
FROM python:3.9.12-slim-bullseye

# set author
LABEL maintainer "Ludovic Cottret <ludovic.cottret@inrae.fr>"

# set encoding
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8

ENV DEBIAN_FRONTEND=noninteractive


RUN useradd -ms /bin/bash galaxy_user
# RUN usermod -aG sudo galaxy_user

# Install dependencies
RUN apt-get update \
    && apt-get install -y \
    curl \
    sudo \
	build-essential \
	libseccomp-dev \
	autoconf \
    automake \
    cryptsetup \
    fuse2fs \
    git \
    fuse \
    libfuse-dev \
    libglib2.0-dev \
    libseccomp-dev \
    libtool \
    pkg-config \
    runc \
    squashfs-tools \
    squashfs-tools-ng \
    uidmap \
    wget \
    zlib1g-dev

# Install planemo
RUN pip install --upgrade pip \
    && pip install planemo

SHELL ["/bin/bash", "-c"]

# Install GO
RUN \
    export VERSION=1.17.2 OS=linux ARCH=amd64 \
    && wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz \
    && tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz \
    && rm go$VERSION.$OS-$ARCH.tar.gz \
    && echo 'export PATH=/usr/local/go/bin:$PATH' >> ~/.bashrc \
    && source ~/.bashrc

# Install Singularity
RUN \
    echo 'export PATH=/usr/local/go/bin:$PATH' >> ~/.bashrc \
    && source ~/.bashrc \
    && export VERSION=3.10.0 \
    && wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz \
    && tar -xzf singularity-ce-${VERSION}.tar.gz \  
    && cd singularity-ce-${VERSION} \
    &&    ./mconfig \
    && make -C builddir \
    && make -C builddir install \
	&& cd .. \
	&& rm -rf singularity-ce-${VERSION} singularity-ce-${VERSION}.tar.gz

# Clean

RUN apt-get clean autoclean \
	&&  apt-get autoremove --yes \
	&&  rm -rf /var/lib/{apt,dpkg,cache,log}/ \
	&&  rm -rf /usr/bin/X11 \
	&&  rm -rf /tmp/*  


# USER galaxyuser

CMD []

