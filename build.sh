#!/bin/bash

rm -rf build
mkdir -p build
cp -RL tools/*/* build

# for every subdirectory in build/
for DIR in build/*/
do
  DIR=${DIR%/}
  # copy macros.xml
  cp tools/macros.xml $DIR
  # copy .shed.yml
  cp .shed.yml $DIR
  BASENAME=$(basename $DIR)
  # name must be in lower case
  NAME=$(echo $BASENAME | tr '[:upper:]' '[:lower:]')
  # add 'met4j_' prefix to the name
  NAME="met4j_$NAME"
  # replace @REPLACE_BY_NAME@ by the name of the subdirectory
  sed -i "s|@REPLACE_BY_NAME@|$NAME|g" $DIR/.shed.yml
  # find the content of the <description> tag in the yml file
  description=$(grep -oP '(?<=<description>).*?(?=</description>)' $DIR/$BASENAME.xml)
  # replace @REPLACE_BY_DESCRIPTION@
  sed -i "s|@REPLACE_BY_DESCRIPTION@|\"$description\"|g" $DIR/.shed.yml
done
