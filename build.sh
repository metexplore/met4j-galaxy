#!/bin/bash

# Check if an argument has been provided for the replacement value
if [ -z "$1" ]; then
  echo "Usage: $0 version_number"
  exit 1
fi

rm -rf build

mkdir -p build/tools

cp -RL tools/*/* build/tools

# cp -RL data build

cp .shed.yml build

# Get the replacement value from the first argument
# Replace all the "/" by "_"
replacement_value=$(echo "$1" | sed 's/\//_/g')

# Find all XML files containing the text "MET4J_VERSION_TEST"
find "build/tools" -type f -name "*.xml" -exec grep -l "MET4J_VERSION_TEST" {} \; | while read file
do
  # Replace the text "MET4J_VERSION_TEST" with the replacement value specified in the program argument
  sed -i "s/MET4J_VERSION_TEST/$replacement_value/g" "$file"
done